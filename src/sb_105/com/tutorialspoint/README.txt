After https://www.tutorialspoint.com/spring/spring_applicationcontext_container.htm

For this SPRING XML (HelloWorld) project you need libraries for your Computer: 

        1x commons-logging-1.1.1
       20x spring-aop-4.1.6.RELEASE (or x.x.x.RELEASE )

Steps for a Spring XML project on your computer:

1; Setup your spring environment:

   https://www.tutorialspoint.com/spring/spring_environment_setup.htm
   Install your JDK 
   Install Apache Common Logging API                https://commons.apache.org/proper/commons-logging/download_logging.cgi 
   Install (Eclipse) IDE
   Setup Spring Framework Libraries for yourself  https://repo.spring.io/release/org/springframework/spring/ 


2; Do it in your IDE:

   File → New → Project >  Java Project,  Name:= HelloSpring
   Add required libraries: 
         Project \ Properties / Build Path → Configure Build Path / Add External JARs button 
         available under the Libraries tab to add the following core JARs 
         from Spring Framework and Common Logging installation directories: 

                 1x Apache Common Logging API 
                 20x spring-......-5.1.2.RELEASE (or x.x.x.RELEASE )

          src \ New Package := com.tutorialspont /

                     1; HelloWorld.java (POJO)

                     2; MainApp.java

          src / Beans.xml (should be available in CLASSPATH)
